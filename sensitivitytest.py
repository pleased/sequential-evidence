import matplotlib.pyplot as plt
import numpy as np
import os
import re
import time

from algs.sgais import SGAIS
from exp import Experiment
from models.gmm import GMM


def alphanum_order(tup):
    def conv(text):
        return float(text) if text.isdigit() else text

    key = tup[0]

    return [conv(c) for c in re.split(r'([-+]?[0-9]*\.?[0-9]*)', key)]


class SensitivityExp(Experiment):
    def __init__(self, name, *,
                 data_size: int = 1_000_000,
                 n_iter=None):
        super().__init__(f'gmm-sensitivity-{name}', data_size=data_size, n_iter=n_iter)
        self.sgais_runs = dict()

        if not os.path.isdir(f'./data/{self.name}/sais'):
            os.makedirs(f'./data/{self.name}/sais')

    def model(self, n_samples=1):
        return GMM(n_components=5, dimensions=2, n_samples=n_samples)

    def run(self):
        for fname in os.listdir(self.file('sais/')):
            label = fname.replace('.out', '')
            if label not in self.sgais_runs:
                self.sgais_runs[label] = np.loadtxt(self.file(f'sais/{fname}'))

        if os.path.isfile(self.file('ns.out')):
            self.ns_out = np.loadtxt(self.file('ns.out'))
        else:
            self.run_ns()

        if os.path.isfile(self.file('ais.out')):
            self.ais_out = np.loadtxt(self.file('ais.out'))
        else:
            self.run_ais()

    def run_sgais(self, label, *,
                  particles=10,
                  ess_ratio=0.5,
                  lr=0.1,
                  burnin=20,
                  batch_size=500):
        args = (particles, ess_ratio, lr, burnin)
        if label in self.sgais_runs:
            return
        print('running sequential ais with args: ', args)
        sgais = SGAIS(self.get_data(), self.model(particles),
                      batch_size=batch_size,
                      ess_ratio=ess_ratio,
                      lr=lr)

        def at_exit(sgais_out):
            sgais_out = np.array(sgais_out)
            self.sgais_runs[label] = sgais_out
            np.savetxt(self.file(f'sais/{label}.out'), sgais_out)
            print(f'time {sgais_out[-1, 1]}')
            print('sequential ais done')

        sais_out = []
        t = time.time()
        try:
            for n, log_z, n_steps in sgais.run(burnin=burnin):
                sais_out += [(n, time.time() - t, float(log_z), n_steps)]
                # + tuple(float(w) for w in sais.log_weights)]

        except KeyboardInterrupt as error:
            at_exit(sais_out)
            raise error

        at_exit(sais_out)

    def plot_logz(self):
        fig = plt.figure()
        plt.tight_layout()

        def plot(ax, ge=0.0):
            n = self.ns_out[:, 0]
            z = self.ns_out[n >= ge, 2]
            n = n[n >= ge]
            ax.plot(n, z / n, 'g--', alpha=0.3)

            n = self.ais_out[:, 0]
            z = self.ais_out[n >= ge, 2]
            n = n[n >= ge]
            ax.plot(n, z / n, 'r--', alpha=0.3)

            for label, run in sorted(self.sgais_runs.items(), key=alphanum_order):
                n = run[:, 0]
                z = run[n >= ge, 2]
                n = n[n >= ge]
                ax.plot(n, z / n, label=label)

        ax = plt.gca()
        ax.set_xscale('log')
        ax.set_xlabel('data set size $N$')
        ax.set_ylabel(r'$\log\mathcal{Z}/N$')
        plot(ax)
        ax.legend(loc='lower left')

        inset = ax.inset_axes([0.45, 0.03, 0.52, 0.6])
        inset.set_xscale('log')
        inset.set_xticklabels('', minor=True)
        inset.set_xticklabels('', minor=False)
        inset.set_yticklabels('', minor=True)
        inset.set_yticklabels('', minor=False)
        inset.set_axisbelow(False)
        plot(inset, ge=4e5)

        ax.indicate_inset_zoom(inset, edgecolor='0.2')

        plt.savefig(f'figs/{self.name}-logz.png', dpi=200)
        plt.close(fig)

    def plot_time(self):
        fig = plt.figure()
        plt.tight_layout()
        plt.xlabel('data set size $N$')
        plt.ylabel('time ($s$)')
        ax = plt.gca()
        ax.xaxis.set_major_formatter(self.time_tick_formater)

        plt.plot(self.ns_out[:, 0], self.ns_out[:, 1], 'g--', alpha=0.3)
        plt.plot(self.ais_out[:, 0], self.ais_out[:, 1], 'r--', alpha=0.3)

        for label, run in sorted(self.sgais_runs.items(), key=alphanum_order):
            plt.plot(run[:, 0], run[:, 1], label=label)

        plt.legend(loc='upper left')
        plt.savefig(f'figs/{self.name}-time.png', dpi=300)
        plt.close(fig)

    def plot_annealing_steps(self):
        fig = plt.figure()
        plt.tight_layout()
        plt.xlabel('data set size $N$')
        plt.ylabel('annealing steps')
        ax = plt.gca()
        ax.xaxis.set_major_formatter(self.time_tick_formater)
        plt.xlim((0, 50_000))

        for label, run in sorted(self.sgais_runs.items(), key=alphanum_order):
            plt.plot(run[:, 0], run[:, 3], alpha=0.7, label=label)

        plt.legend()
        plt.savefig(f'figs/{self.name}-steps.png', dpi=300)
        plt.close(fig)


def main():
    exp = SensitivityExp('M')
    exp.run()
    for M in (5, 10, 15, 20, 25):
        exp.run_sgais(f'M = {M}',
                      particles=M,
                      ess_ratio=0.5,
                      lr=0.1,
                      burnin=20)
    exp.plot()

    exp = SensitivityExp('ESS')
    exp.run()
    for ess in (0.1, 0.2, 0.4, 0.5, 0.6, 0.75):
        exp.run_sgais(f'ESS = {ess} M',
                      particles=10,
                      ess_ratio=ess,
                      lr=0.1,
                      burnin=20)
    exp.plot()

    exp = SensitivityExp('lr')
    exp.run()
    for lr in (0.5, 0.1, 0.05, 0.01, 0.001):
        exp.run_sgais(f'lr = {lr}',
                      particles=10,
                      ess_ratio=0.5,
                      lr=lr,
                      burnin=20)
    exp.plot()

    exp = SensitivityExp('burnin')
    exp.run()
    for burnin in (1, 5, 10, 15, 20):
        exp.run_sgais(f'burnin = {burnin}',
                      particles=10,
                      ess_ratio=0.5,
                      lr=0.1,
                      burnin=burnin)
    exp.plot()

    exp = SensitivityExp('lr-burnin')
    exp.run()
    for lr in (2.0, 1.0, 0.5, 0.1, 0.05):
        exp.run_sgais(f'lr = {lr}',
                      particles=10,
                      ess_ratio=0.5,
                      lr=lr,
                      burnin=int(np.ceil(2/lr)))
    exp.plot()

    exp = SensitivityExp('batch-size')
    exp.run()
    for batch_size in (1000, 500, 100, 50, 25):
        print(f'batch size = {batch_size}')
        exp.run_sgais(f'batch-size = {batch_size}',
                      particles=10,
                      ess_ratio=0.5,
                      lr=0.1,
                      burnin=20,
                      batch_size=batch_size)
    exp.plot()


if __name__ == '__main__':
    main()
