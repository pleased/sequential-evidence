import matplotlib.pyplot as plt
import numpy as np
import torch

from model import Model
from algs.nestedsampling import NestedSampler, SGHMCParticle, RejectionSampler
from algs.sequential import SequentialEstimator
from torch import Tensor


def exact(x):
    x_sq_sum = (x * x).sum()
    x_sum_sq = x.sum() ** 2
    n = x.size(0)
    logZ = -n/2 * np.log(2 * np.pi) - 0.5 * np.log(n + 1)
    logZ += x_sum_sq / (2*n + 2) - x_sq_sum/2
    return logZ


class Gaussian(Model):
    def sample_prior(self, n_samples: int = 1):
        return torch.randn(n_samples),

    def log_p(self, data: Tensor, params):
        mu, = params
        z = data[:, np.newaxis] - mu[np.newaxis, :]
        return -0.5 * z * z - 0.5 * np.log(2*np.pi)

    def log_prior(self, params):
        mu, = params
        return -0.5 * mu * mu - 0.5 * np.log(2*np.pi)

    def sample_data(self, n_samples: int = 1000) -> Tensor:
        mu, = self.sample_prior()
        return torch.randn(n_samples) + mu.reshape(1)


def main():
    gauss = Gaussian()
    data = gauss.sample_data(1000)
    seq = SequentialEstimator(data, gauss,
                              lr=0.1,
                              chunk_size=20, pretrain=0)
    ns = []
    zs = []
    uncert = []

    for n, logz in seq.run(burnin=10):
        ns1 = NestedSampler([
            SGHMCParticle(data[:n],
                          gauss, lr=0.01) for _ in range(2)
        ])
        ns2 = NestedSampler([
            RejectionSampler(data[:n],
                             gauss) for _ in range(10)
        ])
        ns1.run()
        ns2.run()
        z_exact = exact(data[:n])
        print(n, logz, ns1.logZ, ns2.logZ, z_exact)

        zs += [np.array([logz, ns1.logZ, ns2.logZ, z_exact])]
        uncert += [ns2.uncertainty]
        ns += [n]

    ns = np.array(ns)
    uncert = np.array(uncert) / ns
    z_per_n = np.array(zs) / ns.reshape(-1, 1)

    plt.plot(ns, z_per_n)
    plt.fill_between(ns, z_per_n[:, 2] + uncert, z_per_n[:, 2] - uncert)
    plt.legend(['seq', 'ns1', 'ns2', 'exact'])
    plt.show()


if __name__ == '__main__':
    main()
