import torch
import torch.nn as nn

from models.model import Model
from torch.distributions import Normal


class LogReg(Model):
    def __init__(self, in_dim: int = 1,
                 out_dim: int = 2, *,
                 n_samples=1):
        super().__init__()
        self.in_dim = in_dim
        self.out_dim = out_dim
        self.weight = nn.Parameter(torch.randn((n_samples, in_dim, out_dim)))
        self.bias = nn.Parameter(torch.randn(n_samples, out_dim))
        self.prior = Normal(0, 1)

    def log_p(self, data):
        x, y = data

        logits = self(x)
        y = y.reshape(-1, 1, 1).expand(-1, logits.size(1), 1)

        return logits.gather(2, y).squeeze(dim=-1)

    def forward(self, x):
        x = x[:, None, None, :]
        logits = torch.matmul(x, self.weight).squeeze(2) + self.bias
        return logits.log_softmax(dim=-1)

    def log_prior(self):
        return self.prior.log_prob(self.weight).sum(dim=-1).sum(dim=-1) + \
               self.prior.log_prob(self.bias).sum(dim=-1)

    def sample_data(self, n_samples: int = 1000):
        with torch.no_grad():
            x = torch.randn(n_samples, self.in_dim)
            log_p = self.forward(x).squeeze(dim=1)
            y = torch.multinomial(log_p.exp(), 1)

        return x, y

