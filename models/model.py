import torch.nn as nn


class Model(nn.Module):
    def log_p(self, *data):
        return self(*data)

    def log_prior(self):
        raise NotImplementedError

    def forward(self, *data):
        raise NotImplementedError

    def sample_data(self, n_samples: int = 1000):
        raise NotImplementedError

