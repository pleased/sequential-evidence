import numpy as np
import torch
import torch.nn as nn

from models.model import Model
from torch.distributions import Normal


class LinearModel(Model):
    def __init__(self, in_dim: int, *, sigma: float = 1, n_samples=1):
        super().__init__()
        self.sigma = sigma
        self.in_dim = in_dim
        self.weight = nn.Parameter(torch.randn((in_dim, n_samples)))
        self.bias = nn.Parameter(torch.randn(n_samples))

    def log_p(self, data):
        x, y = data

        mu = self(x)
        norm = Normal(mu, self.sigma)

        return norm.log_prob(y)

    def forward(self, x):
        return torch.matmul(x, self.weight) + self.bias

    def log_prior(self):
        norm = Normal(0, 1)
        return norm.log_prob(self.weight).sum(dim=0) + \
            norm.log_prob(self.bias)

    def sample_data(self, n_samples: int = 1000):
        x = torch.randn(n_samples, self.in_dim)
        with torch.no_grad():
            mu = torch.matmul(x, self.weight[:, :1]) + self.bias[:1]
            y = Normal(mu, self.sigma).sample()
        return x, y

    def exact(self, data):
        x, y = data
        n = x.size(0)

        z = y / self.sigma
        X = torch.cat((x, torch.ones_like(z)), dim=1) / self.sigma

        I = torch.eye(self.in_dim + 1, self.in_dim + 1, dtype=X.dtype)
        A = I + (X.transpose(0, 1) @ X)
        zX = z.transpose(0, 1) @ X

        logz = - n/2 * np.log(2 * np.pi * self.sigma * self.sigma)
        logz -= torch.det(A).log() / 2
        logz -= (z * z).sum() / 2
        logz += (zX @ A.inverse() @ zX.transpose(0, 1)).squeeze() / 2

        return logz

