from models.gmm import GMM
from exp import Experiment


class GMMExp(Experiment):
    def __init__(self, *,
                 data_size: int = 1_000_000,
                 n_iter=None):
        super().__init__('gmm', data_size=data_size, n_iter=n_iter)

    def model(self, n_samples=1):
        return GMM(n_components=5, dimensions=2, n_samples=n_samples)


def main():
    exp = GMMExp()
    exp.run()
    exp.plot()


if __name__ == '__main__':
    main()

