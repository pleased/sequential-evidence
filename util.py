
import numpy as np
import torch
import torch.utils.data as udata


def log_ess(log_w):
    log_sum = log_w.logsumexp(dim=0)
    log_sum_2 = (2*log_w).logsumexp(dim=0)
    # sum(w_i)^2 / sum(w_i^2)
    ess = 2 * log_sum - log_sum_2
    return ess


def bisection_search(f, a, b, eps=1e-4):
    mid = (a + b) / 2
    fa, fm = f(a), f(mid)

    while b - a > eps:
        if torch.sign(fa) == torch.sign(fm):
            a, fa = mid, fm
            mid = (a + b) / 2
            fm = f(mid)
        else:
            b = mid
            mid = (a + b) / 2
            fm = f(mid)

    return mid, fm
