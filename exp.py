import matplotlib.pyplot as plt
from models.model import Model
import numpy as np
import os
import time
import torch
import torch.utils.data as dutils

from algs.sgais import SGAIS
from algs.nestedsampling import NestedSampler, SGHMCParticle
from algs.ais import AIS

from matplotlib.ticker import FuncFormatter

plt.rcParams.update({'font.size': 12, 'figure.autolayout': True})
torch.set_num_threads(1)


def log_iter(start, end, step):
    yield start

    x = np.log2(start)
    log_end = np.log2(end)

    while x < log_end:
        x += step
        yield np.min((np.exp2(x), end))


def thousands(x, pos):
    return f'{int(x/1000)}K'


class Experiment(object):
    def __init__(self, name: str, *,
                 data_size: int = 1_000_000,
                 n_iter=None):
        if n_iter is None:
            n_iter = (int(n) for n in log_iter(100, data_size, 0.25))

        self.name = name
        self.data_size = data_size
        self.n_iter = tuple(n_iter)
        self.time_tick_formater = FuncFormatter(thousands)

        self.data = None
        self.ns_out = None
        self.ais_out = None
        self.sgais_out = None

        if not os.path.isdir(f'./data/{self.name}'):
            os.makedirs(f'./data/{self.name}')

    def model(self, n_samples=1) -> Model:
        raise NotImplementedError

    def file(self, fname: str) -> str:
        return f'./data/{self.name}/{fname}'

    def run(self):
        if os.path.isfile(self.file('ns.out')):
            self.ns_out = np.loadtxt(self.file('ns.out'))
        else:
            self.run_ns()

        if os.path.isfile(self.file('ais.out')):
            self.ais_out = np.loadtxt(self.file('ais.out'))
        else:
            self.run_ais()

        if os.path.isfile(self.file('sais.out')):
            self.sgais_out = np.loadtxt(self.file('sais.out'))
        else:
            self.run_sgais()

    def get_data(self):
        if self.data is None:
            if os.path.isfile(self.file('data.pt')):
                self.load_data()
            else:
                self.make_data()
        return self.data

    def make_data(self):
        torch.manual_seed(1234)
        np.random.seed(4321)

        self.data = self.model().sample_data(self.data_size)

        torch.save(self.data, self.file('data.pt'))
        if type(self.data) is tuple:
            self.data = dutils.TensorDataset(*self.data)

        print(f"data writen to file '{self.file('data.pt')}'")

    def load_data(self):
        data = torch.load(self.file('data.pt'))

        if type(data) is tuple:
            data = tuple(d[:self.data_size] for d in data)
            self.data = dutils.TensorDataset(*data)
        else:
            self.data = data[:self.data_size]

    def run_ns(self, *args, **kwargs):
        print('running nested sampler')
        ns_out = []

        try:
            for n in self.n_iter:
                sampler = NestedSampler([
                    SGHMCParticle(self.get_data()[:n], self.model(1),
                                  n_steps=20) for _ in range(2)
                ])
                t = time.time()
                steps = sampler.run()
                ns_out += [(n, time.time() - t, sampler.logZ,
                            sampler.uncertainty, steps)]
                print(ns_out[-1])

        except KeyboardInterrupt:
            pass

        self.ns_out = np.array(ns_out)
        np.savetxt(self.file('ns.out'), self.ns_out)
        print('nested sampler done')

    def run_ais(self, *args, **kwargs):
        print('running ais')
        ais_out = []

        n_iter = list(n for n in self.n_iter if n < 100_000) + [100_000]

        try:
            for n in n_iter:
                ais = AIS(self.get_data()[:n], self.model(10),
                          lr=0.1/n)
                t = time.time()
                steps = ais.run(burnin=20)
                ais_out += [(n, time.time() - t, ais.logZ, steps)]
                print(ais_out[-1])

        except KeyboardInterrupt:
            pass

        self.ais_out = np.array(ais_out)
        np.savetxt(self.file('ais.out'), self.ais_out)
        print('ais done')

    def run_sgais(self, *args, **kwargs):
        print('running sequential ais')
        sais = SGAIS(self.get_data(), self.model(10),
                     # ess_ratio=0.1,
                     lr=0.1)

        sais_out = []
        t = time.time()
        try:
            for n, log_z, n_steps in sais.run(burnin=20):
                sais_out += [(n, time.time() - t, float(log_z), n_steps)]

        except KeyboardInterrupt:
            pass

        self.sgais_out = np.array(sais_out)
        np.savetxt(self.file('sais.out'), self.sgais_out)
        print(f'time {self.sgais_out[-1, 1]}')
        print('sequential ais done')

    def plot_exact(self, ax, ge=0.0):
        pass

    def plot_logz(self):
        fig = plt.figure()
        plt.tight_layout()

        def plot(ax, ge=0.0):
            self.plot_exact(ax, ge=ge)

            n = self.ns_out[:, 0]
            z = self.ns_out[n >= ge, 2]
            n = n[n >= ge]
            ax.plot(n, z / n, 'g', label='ns')

            n = self.ais_out[:, 0]
            z = self.ais_out[n >= ge, 2]
            n = n[n >= ge]
            ax.plot(n, z / n, 'r', label='ais')

            n = self.sgais_out[:, 0]
            z = self.sgais_out[n >= ge, 2]
            n = n[n >= ge]
            ax.plot(n, z / n, 'b', label='sgais')

        ax = plt.gca()
        ax.set_xscale('log')
        ax.set_xlabel('data set size $N$')
        ax.set_ylabel(r'$\log\mathcal{Z}/N$')
        plot(ax)
        ax.legend(loc='upper left')

        inset = ax.inset_axes([0.45, 0.03, 0.52, 0.6])
        inset.set_xscale('log')
        inset.set_xticklabels('', minor=True)
        inset.set_xticklabels('', minor=False)
        inset.set_yticklabels('', minor=True)
        inset.set_yticklabels('', minor=False)
        inset.set_axisbelow(False)
        plot(inset, ge=1e4)

        ax.indicate_inset_zoom(inset, edgecolor='0.2')

        plt.savefig(f'figs/{self.name}-logz.png', dpi=200)
        plt.close(fig)

    def plot_time(self):
        fig = plt.figure()
        plt.tight_layout()
        plt.xlabel('data set size $N$')
        plt.ylabel('time ($s$)')
        ax = plt.gca()
        ax.xaxis.set_major_formatter(self.time_tick_formater)

        plt.plot(self.ns_out[:, 0], self.ns_out[:, 1], 'g', label='ns')
        plt.plot(self.ais_out[:, 0], self.ais_out[:, 1], 'r', label='ais')
        plt.plot(self.sgais_out[:, 0], self.sgais_out[:, 1], 'b', label='sgais')

        plt.legend()
        plt.savefig(f'figs/{self.name}-time.png', dpi=300)
        plt.close(fig)

    def plot_annealing_steps(self):
        fig = plt.figure()
        plt.tight_layout()
        plt.xlabel('data set size $N$')
        plt.ylabel('mcmc steps/$N$')
        ax = plt.gca()
        ax.xaxis.set_major_formatter(self.time_tick_formater)
        plt.xlim((0, 50_000))

        plt.plot(self.ns_out[:, 0], self.ns_out[:, 4], 'g', label='ns')
        plt.plot(self.ais_out[:, 0], self.ais_out[:, 3], 'r', label='ais')
        plt.plot(self.sgais_out[:, 0], self.sgais_out[:, 3], 'b', label='sgais')

        plt.legend()
        plt.savefig(f'figs/{self.name}-steps.png', dpi=300)
        plt.close(fig)

    def plot(self):
        self.plot_logz()
        self.plot_time()
        self.plot_annealing_steps()

