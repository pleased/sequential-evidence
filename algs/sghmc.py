import torch
from torch.optim.optimizer import Optimizer, required


class SGHMC(Optimizer):
    def __init__(self, params, lr=required, alpha=0.1):
        if lr is not required and lr < 0.0:
            raise ValueError("Invalid learning rate: {}".format(lr))
        if alpha <= 0.0 or alpha > 1.0:
            raise ValueError("Invalid alpha value: {}".format(alpha))

        defaults = dict(lr=lr, alpha=alpha)
        super().__init__(params, defaults)

    def step(self, energy, loss_multiplier=1):
        self.zero_grad()
        loss = energy()
        loss.backward()

        self.update_momentum(loss_multiplier)
        self.step_forward()

        return loss

    def resample_momentum(self):
        for group in self.param_groups:
            lr = group['lr']
            std = lr ** 0.5
            for p in group['params']:
                if p.grad is not None:
                    param_state = self.state[p]
                    param_state['momentum'] = std * torch.randn_like(p.data)

    def update_momentum(self, loss_multiplier):
        for group in self.param_groups:
            alpha = group['alpha']
            lr = group['lr']
            std = (2 * alpha * lr / loss_multiplier) ** 0.5

            for p in group['params']:
                if p.grad is None:
                    continue
                param_state = self.state[p]
                if len(param_state) < 1:
                    d_p = param_state['momentum'] = \
                        ((lr/loss_multiplier) ** 0.5) * torch.randn_like(p.data)
                else:
                    d_p = param_state['momentum']
                d_p.mul_(1 - alpha).add_(-lr, p.grad.data)
                d_p.add_(std, torch.randn_like(p.data))

    def step_forward(self, steps=1):
        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                param_state = self.state[p]
                d_p = param_state['momentum']
                p.data.add_(steps, d_p)


class ConstrainedSGHMC(SGHMC):
    def __init__(self, params, lr=required, alpha=0.1):
        super().__init__(params, lr, alpha)

    def step(self, energy, constraint_fn=None, loss_multiplier=1):
        loss = super().step(energy, loss_multiplier)

        if constraint_fn is None or constraint_fn() >= 0:
            return loss

        self.step_back()
        self.zero_grad()
        constraint_fn().backward()
        self.reflect_momentum()

        # make sure can't go left, only right, otherwise reverse
        # TODO maybe we only need to go right? langevin dynamics does
        # not necessarily need to satisfy detailed balance
        self.step_back()  # left
        if constraint_fn() < 0:
            # go right
            self.step_forward(steps=2)  # 2 right
            if constraint_fn() >= 0:
                return loss
            self.step_back()  # left
        else:
            self.step_forward()

        self.restore_and_reverse_momentum()
        self.step_forward()  # backwards
        if constraint_fn() < 0:
            self.step_back()  # back where we started
            # TODO deal with increasing momentum when we can't
            # move for multiple steps
            # print('warning, did not move')

        return loss

    def update_momentum(self, loss_multiplier):
        super().update_momentum(loss_multiplier)
        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                param_state = self.state[p]
                if len(param_state) < 2:
                    param_state['d_p'] = torch.empty_like(p.data)
                param_state['d_p'].copy_(param_state['momentum'])

    def step_back(self):
        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                param_state = self.state[p]
                d_p = param_state['momentum']
                p.data.sub_(d_p)

    def reflect_momentum(self):
        n_dot_n = 0
        v_dot_n = 0

        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                param_state = self.state[p]
                g = p.grad.data
                v_dot_n += (g * param_state['momentum']).sum()
                n_dot_n += (g * g).sum()

        a = 2 * v_dot_n / n_dot_n
        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                param_state = self.state[p]
                param_state['momentum'].sub_(a, p.grad.data)

    def restore_and_reverse_momentum(self):
        for group in self.param_groups:
            for p in group['params']:
                if p.grad is None:
                    continue
                param_state = self.state[p]
                d_p = param_state['momentum']
                d_p.copy_(param_state['d_p'])
                d_p.mul_(-1)
