import numpy as np
import torch
import util

from models.model import Model
from algs.sghmc import SGHMC


class SGAIS(object):
    def __init__(self, data, particles: Model, *,
                 batch_size: int = 500,
                 lr: float = 1e-3,
                 alpha: float = 0.2,
                 ess_ratio: float = 0.5):
        self.data = data
        self.cur_beta = 0
        self.particles = particles
        self.log_weights = 0
        self.ess_ratio = ess_ratio

        self.sghmc = SGHMC(self.particles.parameters(), lr=lr, alpha=alpha)
        self.logZ = 0
        self.batch_size = batch_size
        self.current_index = 0

    def energy(self):
        log_prior = self.particles.log_prior().sum()
        energy = -log_prior

        chunk = self.data[self.current_index: self.current_index + self.batch_size]
        energy -= self.cur_beta * self.particles.log_p(chunk).sum()
        energy /= self.current_index + self.batch_size

        if self.current_index > 0:
            batch = self.data[np.random.choice(self.current_index, self.batch_size)]
            energy -= self.current_index/(self.current_index + self.batch_size) * \
                self.particles.log_p(batch).mean(dim=0).sum()

        return energy

    def step_sampler(self):
        self.sghmc.step(self.energy, self.current_index + self.batch_size)

    def step(self, burnin: int = 10):
        chunk = self.data[self.current_index: self.current_index + self.batch_size]

        def f(d):
            return util.log_ess(d * log_p).exp() - self.ess_ratio * log_p.size(0)

        self.cur_beta = 0

        n_steps = 0

        while self.cur_beta < 1:
            n_steps += 1
            with torch.no_grad():
                log_p = self.particles.log_p(chunk).sum(dim=0)

            delta, fm = util.bisection_search(f, 0, 1 - self.cur_beta, 1e-4)
            if self.cur_beta + delta + 1e-4 >= 1:
                delta = 1 - self.cur_beta
            self.log_weights += delta * log_p
            # print(self.current_index, self.cur_beta, delta, fm + self.optimal_ess, n_steps)

            self.cur_beta += delta
            for _ in range(burnin):
                self.step_sampler()

        self.current_index += self.batch_size
        return n_steps

    def run(self, burnin: int = 10):
        while self.current_index < len(self.data):
            n_steps = self.step(burnin)
            self.logZ = self.log_weights.logsumexp(dim=0) - np.log(self.log_weights.size(0))
            yield (self.current_index, float(self.logZ), n_steps)

