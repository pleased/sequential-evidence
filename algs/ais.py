import numpy as np
import torch
import util

from algs.sghmc import SGHMC
from models.model import Model


class AIS(object):
    def __init__(self, data, particles: Model, *,
                 lr: float = 1e-3,
                 alpha: float = 0.2,
                 ess_ratio: float = 0.5):
        self.data = data
        self.cur_beta = 0
        self.particles = particles
        self.log_weights = 0
        self.ess_ratio = ess_ratio

        self.sghmc = SGHMC(self.particles.parameters(), lr=lr, alpha=alpha)
        self.logZ = 0

    def log_like(self):
        return self.particles.log_p(self.data).sum(dim=0)

    def energy(self):
        if self.cur_beta == 0:
            return -self.particles.log_prior().sum()

        return -self.cur_beta * self.log_like().sum() - \
            self.particles.log_prior().sum()

    def step_sampler(self):
        self.sghmc.step(self.energy)

    def step(self, burnin: int = 10):
        log_like = self.log_like()

        def f(d):
            return util.log_ess(d * log_like).exp() - self.ess_ratio * log_like.size(0)

        with torch.no_grad():
            delta, fd = util.bisection_search(f, 0, 1 - self.cur_beta)
            self.log_weights += delta * log_like

        self.cur_beta += delta
        for _ in range(burnin):
            self.step_sampler()

    def run(self, burnin: int = 10):
        n_steps = 0

        while self.cur_beta < 1:
            self.step(burnin)
            n_steps += 1

        self.logZ = self.log_weights.logsumexp(dim=0).numpy() - np.log(self.log_weights.size(0))

        return n_steps
