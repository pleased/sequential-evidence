import matplotlib.pyplot as plt
import numpy as np
import os
import torch

from algs.sgais import SGAIS
from models.gmm import GMM
from matplotlib.ticker import FuncFormatter


def model(components):
    return GMM(n_components=components, dimensions=2, n_samples=10)


def filename(f):
    return f'data/shift/{f}'


def main():
    if not os.path.isdir('data/shift'):
        os.makedirs('data/shift')

    if not os.path.isfile(filename('data.pt')):
        data = make_data()
        torch.save(data, filename('data.pt'))
    else:
        data = torch.load(filename('data.pt'))

    hist_data(data)

    inorder = run_or_load('inorder', data)
    shuffled = run_or_load('randall', data[torch.randperm(data.size(0))])
    plot(inorder, shuffled)


def run_or_load(label, data):
    runs = dict()

    if not os.path.isdir(filename(label)):
        os.makedirs(filename(label))

    print(label)
    for c in (3, 5, 7):
        if not os.path.isfile(filename(f'{label}/c = {c}.out')):
            runs[c] = run_sgais(data, components=c)
            np.savetxt(filename(f'{label}/c = {c}.out'), runs[c])
        else:
            runs[c] = np.loadtxt(filename(f'{label}/c = {c}.out'))

    return runs


def run_sgais(data, components):
    print(f'sgais components = {components}')
    out = []
    try:
        sgais = SGAIS(data, model(components), lr=0.1)
        for n, logz, steps in sgais.run(burnin=20):
            out += [(n, logz, steps)]
    except KeyboardInterrupt:
        pass
    out = np.array(out)
    print('sgais complete')
    return out


def make_data():
    np.random.seed(12345)
    torch.manual_seed(54321)

    mus = torch.randn(7, 2, dtype=torch.double) * 2
    sigmas = torch.empty_like(mus).uniform_(0.3, 1)

    z1 = np.random.choice(3, 1_000)
    z2 = np.random.choice(5, 9_000)
    z3 = np.random.choice(7, 90_000)
    z = np.concatenate((z1, z2, z3))
    x = torch.randn(len(z), 2, dtype=torch.double) * sigmas[z] + mus[z]

    return x


def hist_data(data):
    data = data.numpy()
    bounds = [
        [data[:, 0].min(), data[:, 0].max()],
        [data[:, 1].min(), data[:, 1].max()]
    ]

    x = data[:1_000]
    fig = plt.figure()
    plt.axis('off')
    plt.hist2d(x[:, 0], x[:, 1], bins=75, range=bounds)
    extent = plt.gca().get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    plt.savefig(f'figs/dist-shift-1k.png', dpi=300, bbox_inches=extent)
    plt.close(fig)

    x = data[1_000:10_000]
    fig = plt.figure()
    plt.axis('off')
    plt.hist2d(x[:, 0], x[:, 1], bins=75, range=bounds)
    extent = plt.gca().get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    plt.savefig(f'figs/dist-shift-9k.png', dpi=300, bbox_inches=extent)
    plt.close(fig)

    x = data[10_000:100_000]
    fig = plt.figure()
    plt.axis('off')
    plt.hist2d(x[:, 0], x[:, 1], bins=75, range=bounds)
    extent = plt.gca().get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    plt.savefig(f'figs/dist-shift-90k.png', dpi=300, bbox_inches=extent)
    plt.close(fig)

    fig = plt.figure()
    plt.axis('off')
    plt.hist2d(data[:, 0], data[:, 1], bins=75, range=bounds)
    extent = plt.gca().get_window_extent().transformed(fig.dpi_scale_trans.inverted())
    plt.savefig(f'figs/dist-shift-100k.png', dpi=300, bbox_inches=extent)
    plt.close(fig)


def plot(inorder, shuffled, colors={3: 'b', 5: 'r', 7:'g'}):
    fig = plt.figure()
    plt.tight_layout()
    plt.xscale('log')
    plt.xlabel('$N$')
    plt.ylabel(r'$\log\mathcal{Z}/N$')

    for c, run in sorted(inorder.items(), key=lambda it: it[0]):
        n = run[:, 0]
        z = run[:, 1]
        plt.plot(n, z / n, colors[c], label=f'{c} components')

    for c, run in sorted(shuffled.items(), key=lambda it: it[0]):
        n = run[:, 0]
        z = run[:, 1]
        plt.plot(n, z / n, colors[c] + '--', label=f'{c} components')

    plt.legend()
    plt.savefig(f'figs/dist-shift.png', dpi=300)
    plt.close(fig)

    fig = plt.figure()
    plt.tight_layout()
    plt.xlabel('$N$')
    plt.ylabel(r'annealing steps')
    plt.xlim(0, 20_000)
    plt.gca().xaxis.set_major_formatter(FuncFormatter(lambda x, _: f'{int(x/1000)}K'))

    for c, run in sorted(inorder.items(), key=lambda it: it[0]):
        n = run[:, 0]
        z = run[:, 2]
        plt.plot(n, z, colors[c], label=f'{c} components')

    plt.legend()
    plt.savefig(f'figs/dist-shift-steps.png', dpi=300)
    plt.close(fig)


if __name__ == '__main__':
    main()
