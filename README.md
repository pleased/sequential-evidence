# Sequential Evidence

This repository contains code for the experiments in our manuscript submitted to
Entropy titled

- Stochastic Gradient Annealed Importance Sampling for Efficient Online Marginal
    Likelihood Estimation

The code for this paper is under the tag `entropy`. The code for our previous
conference paper submitted to MaxEnt 2019 titled

- A Sequential Marginal Likelihood Approximation Using Stochastic Gradients

can be found under the tag `maxent`.
