import matplotlib.pyplot as plt
import numpy as np
import os

from models.linear import LinearModel
from exp import Experiment


class LinExp(Experiment):
    def __init__(self, *,
                 data_size: int = 1_000_000,
                 n_iter=None):
        super().__init__('lin', data_size=data_size, n_iter=n_iter)
        self.exact = None

    def model(self, n_samples=1):
        return LinearModel(5, n_samples=n_samples)

    def run(self):
        super().run()
        if os.path.isfile(self.file('exact.out')):
            self.exact = np.loadtxt(self.file('exact.out'))
        else:
            self.calc_exact()

    def calc_exact(self):
        model = LinearModel(5)
        zs = []
        for n in self.n_iter:
            zs += [(n, model.exact(self.get_data()[:int(n)]))]

        self.exact = np.array(zs)
        np.savetxt(self.file('exact.out'), self.exact)

    def plot_exact(self, ax, ge=0.0):
        n, z = self.exact[:, 0], self.exact[:, 1]
        z = z[n >= ge]
        n = n[n >= ge]
        ax.plot(n, z/n, 'k', label='exact')


def main():
    exp = LinExp()
    exp.run()
    exp.plot()


if __name__ == '__main__':
    main()
